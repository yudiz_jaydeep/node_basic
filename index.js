var express = require('express'),
	app = express();
port = 3000;
app.listen(port);
console.log('Magic Happens on :', port);

var randomstring = require("randomstring");
var jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');
bodyParser = require('body-parser');
var router = express.Router();
var mysql = require('mysql');
var md5 = require('md5');
var date = new Date();
var urlparser = bodyParser.urlencoded({ extended: true });
var con = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "root",
	database: 'node',
	port: '8889'
});

con.connect(function (error) {
	if (error) throw error;
	console.log("Connected!");
});

app.post('/login', urlparser, function (req, res) {
	con.query("SELECT * FROM tbl_users WHERE vEmail=? AND vPassword=?", [req.body.email, md5(req.body.password)], function (error, result) {
		if (!error) {
			if (result.length > 0) {
				var token = randomstring.generate(date);
				con.query("UPDATE tbl_users SET vToken=? WHERE vEmail=?", [token, result[0].vEmail]);
				console.log("Login Successfully.");
				res.json({ "status": 200, "Message": result, "Updated Token": token });
			} else {
				res.json({ "status": 401, "Message": "Check email or password." });
			}

		}
		else {
			res.json({ "status": 401, "Message": "Fail to login" });
		}
	});
});

app.post('/register', urlparser, function (req, res) {
	con.query("SELECT * FROM tbl_users WHERE vEmail=? AND vUserName=?", [req.body.email, req.body.name], function (error, result) {
		if (!error) {
			if (result.length > 0) {
				res.json({ "status": 401, "Message": "Email and Username already Used" });
			} else {
				con.query("INSERT INTO tbl_users(vUserName,vEmail,vPassword,vAddress) values(?,?,?,?)", [req.body.name, req.body.email, md5(req.body.password), req.body.address], function (error, result) {
					if (error) {
						res.json({ "status": 401, "Message": "Fail to Register" });
					} else {
						res.json({ "status": 200, "Message": "Successfully Registration Done." });
					}
				});
			}
		}
	});
});

app.post('/list', urlparser, function (req, res) {
	var auth = req.headers['authentication'];
	con.query("SELECT * FROM tbl_users WHERE vToken=?", [auth], function (error, result) {
		if (!error) {
			if (result.length > 0) {
				con.query("SELECT * FROM tbl_users WHERE iUserId=? AND eIsActive='y'", [result[0].iUserId], function (error, result2) {
					if (error) {
						res.json({ "status": 401, "Message": "Fail to Retrieve data." });
					}
					else {
						res.json({ "status": 200, "Message": result2 });
					}
				});
			} else {
				res.json({ "status": 401, "Message": "Please login first." });
			}
		}
	});
});

app.post('/update', urlparser, function (req, res) {
	var auth = req.headers['authentication'];
	con.query("SELECT * FROM tbl_users WHERE vToken=?", [auth], function (error, result) {
		if (!error) {
			if (result.length > 0) {
				con.query("SELECT * FROM tbl_users WHERE iUserId=?", [result[0].vUserId], function (error, result2) {
					if (error) {
						res.json({ "status": 401, "Message": "Fail to Retrieve data." });
					}
					else {
						con.query("UPDATE tbl_users  SET vUserName=?,vEmail=?,vAddress=? WHERE vEmail=?", [req.body.name, req.body.email, req.body.address, result[0].vEmail], function (error, result) {
							if (error) {
								res.json({ "status": 401, "Message": "Fail to Update data." });
							}
							else {
								res.json({ "status": 200, "Message": result });
							}
						});
					}
				});
			} else {
				res.json({ "status": 401, "Message": "Please login first." });
			}
		}
	});
});

app.post('/add', urlparser, function (req, res) {
	var auth = req.headers['authentication'];
	con.query("SELECT vToken FROM tbl_users WHERE vToken=?", [auth], function (error, result) {
		if (!error) {
			con.query("SELECT vEmail FROM tbl_users WHERE vEmail=?", [req.body.email], function (error, result) {
				if (!error) {
					if (result.length > 0) {
						res.json({ "status": 401, "Message": "Email Already Registered." });
					} else {
						con.query("INSERT INTO tbl_users(vUserName,vEmail,vPassword,vAddress) values(?,?,?,?)", [req.body.name, req.body.email, md5(req.body.password), req.body.address], function (error, result) {
							if (error) {
								res.json({
									"status": 401, "Message":
									"Fail to Add User"
								});
							} else {
								res.json({ "status": 200, "Message": "Successfully Added" });
							}
						});
					}
				}
			});
		}
		else {
			res.json({ "status": 402, "Message": "Please Login First" })
		}
	});
});
app.post('/delete', urlparser, function (req, res) {
	var auth = req.headers['authentication'];
	con.query("SELECT * FROM tbl_users WHERE vToken=?", [auth], function (error, result) {
		if (!error) {
			if (result.length > 0) {
				con.query("SELECT iUserId FROM tbl_users WHERE iUserId=?", [result[0].iUserId], function (error, result) {
					if (error) {
						res.json({ "status": 401, "Message": "Fail to Retrieve data." });
					} else {
						if (result.length > 0) {
							con.query("DELETE FROM tbl_users WHERE iUserId=?", [req.body.userid]);
							res.json({ "status": 200, "Message": "User has been deleted." });
						} else {
							res.json({ "status": 401, "Message": "error in deletion." });
						}
					}
				});
			} else {
				res.json({ "status": 401, "Message": "Please login first." });
			}
		}
	});
});
app.post('/changeStatus', urlparser, function (req, res) {
	var auth = req.headers['authentication'];
	con.query("SELECT * FROM tbl_users WHERE vToken=?", [auth], function (error, result) {
		if (!error) {
			if (result.length > 0) {
				con.query("SELECT iUserId,eIsActive FROM tbl_users WHERE iUserId=?", [result[0].iUserId], function (error, result) {
					if (error) {
						res.json({
							"status": 401, "Message":
							"FAil to Load data."
						});
					}
					else {
						if (result.length > 0) {
							con.query("UPDATE tbl_users SET eIsActive=? ", [req.body.status], function (error, result) {
								if (error) {
									res.json({ "status": 401, "Message": "Fail to Update data." });
								}
								else {
									res.json({ "status": 200, "Message": result });
								}
							});

						}
					}
				});
			}
			else {
				res.json({ "status": 401, "Message": "Login First" });
			}
		}
	});
});

app.post('/forgotPassword', urlparser, function (req, res) {
	var token = randomstring.generate(date);
	let transporter = nodemailer.createTransport({
		host: 'smtp.gmail.com',
		service: "Gmail",
		port: 465,
		//secure: true, // secure:true for port 465, secure:false for port 587
		auth: {
			user: 'jaydeep.kataria@itindia.co.in',
			pass: '9428425380@j'
		}
	});
	// setup e-mail data with unicode symbols
	var mailOptions = {
		from: '"jaydeep kataria" <jaydeep.kataria@itindia.co.in>', // sender address
		to: req.body.email, // list of receivers
		subject: 'New Password Request', // Subject line
		text: 'Hello there,', // plaintext body
		html: '<p> your new password is </p><b>' + token + '</b>' // html body
	};


	con.query("SELECT * FROM tbl_users WHERE vEmail=?", [req.body.email], function (error, result) {

		// send mail with defined transport object

		if (error) {
			res.json({ "status": 401, "Message": "Error in query." });
			return console.log(error);
		} else {
			if (result.length > 0) {
				transporter.sendMail(mailOptions, function (error, info) {
					if (error) {
						console.log(error);
						res.json({ "status": 401, "Message": "Problem in sending email... Try again..." });
					} else {
						con.query("UPDATE tbl_users SET vPassword=? WHERE vEmail=?", [md5(token), req.body.email], function (error, results) {
							if (!error) {
								res.json({ "status": 200, "Message": "Your new password has been send to your email address." });
							}
						});
					}
				});
			}
			else {
				res.json({ "status": 401, "Message": "Please Check Email Address" });
			}
		}
	});
});
module.exports = app;